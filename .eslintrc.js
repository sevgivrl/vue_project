module.exports = {
    root: true,
    parserOptions: {
        parser: 'babel-eslint',
        sourceType: 'module'
    },
    env: {
        browser: true,
        "commonjs": true,
        "es6": true,
        "jquery": true,
        "node": true,
        "amd": true,
    },
    extends: [
        // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
        // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
        'plugin:vue/essential',
        // https://github.com/standard/standard/blob/master/docs/RULES-en.md
        'standard',
        "eslint:recommended",
        "plugin:requirejs/recommended"
    ],
    // required to lint *.vue files
    plugins: [
        'vue',
        "jquery",
        "requirejs"

    ],
    globals: {
        'ga': true, // Google Analytics
        'cordova': true,
        '__statics': true,
        "$": true,
        "jQuery": true
    },
    // add your custom rules here
    'rules': {
        // allow async-await
        'generator-star-spacing': 'off',

        // allow paren-less arrow functions
        'arrow-parens': 0,
        'one-var': 0,

        'import/first': 0,
        'import/named': 2,
        'import/namespace': 2,
        'import/default': 2,
        'import/export': 2,
        'import/extensions': 0,
        'import/no-unresolved': 0,
        'import/no-extraneous-dependencies': 0,

        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
        "requirejs/no-invalid-define": 2,
        "requirejs/no-multiple-define": 2,
        "requirejs/no-named-define": 2,
        "requirejs/no-commonjs-wrapper": 2,
        "requirejs/no-object-define": 1
    }
}